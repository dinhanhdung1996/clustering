import document
import math
import numpy
import text_clustering2.Cluster
from sklearn.feature_extraction.text import TfidfVectorizer


class KMeans:
    def __init__(self, default_k=10):
        self.word_list_idf = {}
        self.word_list_num = {}
        self.word_list_tf_idf = numpy.array([])
        self.word_list = []
        self.doc_string_list = []
        self.doc_list = []
        self.clusters = []
        self.previous = []
        self.data_vector = numpy.array([])
        self.k = default_k
        self.t = 0

    def add_doc(self, doc_type, string_data, check=False):
        if check:
            if string_data in self.doc_list:
                return
        doc = document.Document()
        doc.doc_type = doc_type
        doc.string_data = string_data
        doc.to_words()
        self.doc_string_list.append(doc.string_data)
        self.doc_list.append(doc)
        self.t += len(doc.word_list)
        for a_word in doc.word_list:
            if a_word not in self.word_list_num:
                self.word_list_num[a_word] = 0
                self.word_list.append(a_word)
            self.word_list_num[a_word] += 1

    def calculate_idf(self):
        for a_word in self.word_list_num:
            self.word_list_idf[a_word] = math.log(len(self.doc_list)/self.word_list_num[a_word])

    # def data_vectorization(self):
    #     (m, n) = self.doc_list.shape
    #     for i in range(n):
    #         for a_word in self.word_list_num:
    #             if a_word not in self.doc_list[0, i].word_list:

    def data_vectorization(self):
        vect = TfidfVectorizer(sublinear_tf=True, max_df=0.5, analyzer='word', vocabulary=self.word_list)
        vect.fit(self.doc_string_list)
        self.data_vector = vect.transform(self.doc_string_list)
        return self.data_vector

    def check_stop(self, previous):
        if len(self.clusters) != len(self.previous):
            print("fa")
            return False
        else:
            for i in range(len(self.clusters)):
                if self.clusters[i] != self.previous[i]:
                    return False
            return True

    def k_means(self):
        test = int((len(self.word_list_num) * len(self.doc_string_list))/self.t)
        print("test = ", test)
        print(self.k)
        self.data_vectorization()
        for i in range(self.k):
            cluster = text_clustering2.Cluster.Cluster(len(self.word_list_num))
            cluster.generate_centroid()
            self.clusters.append(cluster)
        (m, n) = self.data_vector.shape
        count = 0
        while True:
            count += 1
            print(count)
            if count != 1:
                self.previous = list(self.clusters)
            if count == 65:
                break
            for i in range(len(self.clusters)):
                self.clusters[i].clear()
            for i in range(m):
                best_distance = 999999999999
                best_cluster = None
                for j in range(len(self.clusters)):
                    distance = self.clusters[j].distance_to(self.data_vector[i, :].toarray())
                    if distance < best_distance:
                        best_cluster = self.clusters[j]
                        best_distance = distance
                best_cluster.add_point(self.data_vector[i, :].toarray())
                best_cluster.type.append(self.doc_list[i].doc_type)
            for i in range(len(self.clusters)):
                self.clusters[i].calculate_centroid()
                print(self.clusters[i].type)
            # if self.check_stop(self.previous):
            #     break





