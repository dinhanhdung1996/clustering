import math
import numpy
import random


class Cluster:
    def __init__(self, default_size=1000):
        self.size = default_size
        self.centroid = numpy.array([])
        self.points_array = numpy.array([])
        self.points_number = -1
        self.type = []

    def distance_to(self, data_vector):
        if type(data_vector) is not numpy.ndarray:
            return
        # return numpy.linalg.norm(self.centroid - data_vector)
        if self.centroid.size == 0 or data_vector.size == 0:
            return 9999999999999999
        return numpy.linalg.norm(self.centroid - data_vector[0])

    def generate_centroid(self):
        self.centroid = numpy.array([])
        for i in range(self.size):
            self.centroid = numpy.append(self.centroid, numpy.array([random.uniform(0.0, 0.005)]))
        return self.centroid

    def calculate_centroid(self):
        if self.points_array.size == 0:
            return
        self.centroid = numpy.mean(self.points_array, axis=0)
        a = 5

    def add_point(self, data_vector):
        n = self.points_array.size
        if n == 0:
            self.points_array = numpy.append(self.points_array, data_vector[0])
        else:
            self.points_array = numpy.vstack((self.points_array, data_vector[0]))

    def clear(self):
        self.points_array = numpy.array([])
        self.type.clear()

    def __eq__(self, other):
        if type(other) is not Cluster:
            return False
        return numpy.array_equal(self.centroid, other.centroid)

    def __str__(self):
        return str(self.centroid)





