import os
import glob
from xml.dom import minidom
import document
import text_clustering2.Kmeans
import random
import numpy


def main():
    document.Document.get_stop_words("vietnamese-stopwords-dash.txt")
    my_path = os.path.dirname(os.path.abspath(__file__))
    off_path = os.path.join(my_path, "SVTT", "*.xml")
    files = glob.glob(off_path)
    k_means = text_clustering2.Kmeans.KMeans(10)
    for file in files:
        print(file)
        dom = minidom.parse(file)
        rows = dom.getElementsByTagName("row")
        for row in rows:
            if random.uniform(0.0, 1.0) > 0.01:
                continue
            fields = row.getElementsByTagName("field")
            doc_type = fields[0].firstChild.data
            string_data = fields[1].firstChild.data
            k_means.add_doc(doc_type, string_data, False)
    k_means.k_means()
    # for i in range(len(k_means.clusters)):
    #     print(k_means.clusters[i].type)
    # result = k_means.data_vectorization()
    # print(result.todense())
    # print(type(result[0, :].toarray()))
    # print(type(result))

# numpy.set_printoptions(threshold=numpy.nan, precision=3, suppress=False)
main()
